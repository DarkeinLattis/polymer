FROM node:latest

WORKDIR /app

ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
